// Kib2PDF: Easy tool to convert a Kibana dashboard into a PDF file
// Copyright (C) 2018  Jose Manrique Lopez de la Fuente
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


var args = require('system').args;
var page = require('webpage').create();

var waitTime = 15*1000;

page.viewportSize = {width: 1200, height: 800};

page.paperSize = {
  format: 'A4',
  orientation: 'portrait',
  margin: '1cm'
};

page.zoomFactor = .65;

page.open(args[1]+'?embed=true', function(){
  window.setTimeout(function() {
    page.injectJs('jquery-3.3.1.min.js');
    page.evaluate(function (){
      $('filter-bar').css({display:'none'});
      $('.filter-bar').css({display:'none'});
    });
    page.render(args[2]);
    phantom.exit();
  }, waitTime);
});