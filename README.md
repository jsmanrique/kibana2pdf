# Kib2PDF

Kib2pdf stands for *kibana to pdf*. It allows to convert a Kibana dashboard to a
PDF file.

It's a PoC (*proof concept*) to check if it would be feasbile to use Kibana as source
for GrimoireLab reports, instead of [GrimoireLab Manuscripts](https://github.com/chaoss/grimoirelab-manuscripts).

The idea is:
```
Would it be possible to get a PDF file from a given Kibana/Kibiter URL that would
look like a report and not like a dashboard?
```

A good information source has been [this post](https://elatov.github.io/2017/03/kibana-reports-with-phantomjs/).

## Requirements

You need [PhantomJS](http://phantomjs.org/) installed in your machine.

## Usage

I've tried to keep it as simple as possible:

```
$ phantomjs kib2pdf.js [dashboard url] [output PDF filename]
```

For example:

```
$ phantomjs kib2pdf.js https://grimoirelab.biterg.io/app/kibana\#/dashboard/Overview grimoirelab.pdf
```

You'll get a PDF file like [this](grimoirelab.pdf)

As *side effect* of using PhantomJS, if you use `.png` instead of `.pdf` as file
extension, you'll get a PNG file. For example:

```
$ phantomjs kib2pdf.js https://grimoirelab.biterg.io/app/kibana\#/dashboard/Overview grimoirelab.png
```

You'll get:

![](grimoirelab.png)

# License

GPL v3